import { FETCH_DECK_OF_CARDS,
         DEAL_CARDS, DEAL_CARD,
         DEAL_CARD_TO_PLAYER,
         DEAL_CARD_TO_DEALER,
         PLAYER_STAND,
         FLIP_CARD,
         SCORE } from '../actionTypes'

export const startGame = () => {
    return { type: FETCH_DECK_OF_CARDS }
}

export const dealCards = (deckId) => {
  return { type: DEAL_CARDS, deckId }
}

export const dealCard = (deckId) => {
  return { type: DEAL_CARD, deckId }
}

export const playerStand = () => {
  return { type: PLAYER_STAND }
}

export const flipCard = (index) => {
  return { type: FLIP_CARD, index}
}

export const scoreGame = () => {
  return { type: SCORE }
}
