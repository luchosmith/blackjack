import React, { Component } from 'react'
import { render } from 'react-dom'
import { Provider } from "react-redux"
import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootSaga from './sagas'
import mainReducer from './reducers'
import initialState from './initialState'

import Game from './containers/Game'

const sagaMiddleware = createSagaMiddleware()

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(
  mainReducer,
  initialState,
  composeEnhancers(
      applyMiddleware(sagaMiddleware)
  )
)

sagaMiddleware.run(rootSaga)

class App extends Component {
  constructor() {
    super()
    this.store = store
    this.state = this.store.getState()
  }

  render() {
    return (
      <Provider store={this.store}>
        <Game></Game>
      </Provider>
    )
  }
}

render(<App />, document.getElementById('root'))
