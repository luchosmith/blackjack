import { partition } from 'lodash'
import initialState from '../initialState'
import { DEALER,
         PLAYER,
         DECK_OF_CARDS_LOADING,
         FETCH_CARDS_SUCCESS,
         FETCH_CARDS_ERROR, 
         DEAL_CARDS_SUCCESS,
         DEAL_CARD_TO_PLAYER_SUCCESS,
         PLAYER_STAND,
         FLIP_CARD,
         SCORE, 
         DEAL_CARD_TO_DEALER_SUCCESS} from '../actionTypes'


const cardValue = (card) => {
  if(card.faceDown) {
    return 0
  }
  if ( Number.isInteger(Number.parseInt(card.value))) {
    return Number.parseInt(card.value)
  }
  if (card.value === 'ACE') {
    return 1
  }
  return 10
}

const pointsReducer = (points, card) => {
  return points += cardValue(card)
}

const flipcards = (cards) => {
  return cards.map(card => {
    card.faceDown = false
    return card
  })
}

export default function gameReducer(state = initialState.game, action) {

  switch(action.type) {

    case DECK_OF_CARDS_LOADING: {
      return {
        ...state,
        loading: true
      }
    }

    case FETCH_CARDS_SUCCESS: {
      return {
        ...state,
        player: {
          hand: [],
          points: 0
        },
        dealer: {
          hand: [],
          points: 0
        },
        loading: false,
        turn: null,
        winner: null
      }
    }

    case DEAL_CARDS_SUCCESS: {

      let dealerHand = [
        {
          ...action.cards.cards[1]
        },
        {
          ...action.cards.cards[3],
          faceDown: true
        }
      ]

      let playerHand = [
        {
          ...action.cards.cards[0]
        },
        {
          ...action.cards.cards[2],
          faceDown: true
        }
      ]

      return {
        ...state,
        player: {
          hand: playerHand,
          points: playerHand.reduce(pointsReducer, 0)
        },
        dealer: {
          hand: dealerHand,
          points: dealerHand.reduce(pointsReducer, 0)
        },
        turn: PLAYER,
        winner: null
      }
    }

    case DEAL_CARD_TO_PLAYER_SUCCESS: {
      let playerHand = state.player.hand.slice().concat(action.cards.cards[0])
      playerHand = flipcards(playerHand)
      return {
        ...state,
        player: {
          hand: playerHand,
          points: playerHand.reduce(pointsReducer, 0)
        }
      }
    }

    case PLAYER_STAND: {
      let playerHand = flipcards(state.player.hand.slice())
      let dealerHand = flipcards(state.dealer.hand.slice())
      return {
        ...state,
        player: {
          hand: playerHand,
          points: playerHand.reduce(pointsReducer, 0)
        },
        dealer: {
          hand: dealerHand,
          points: dealerHand.reduce(pointsReducer, 0)
        },
        turn: DEALER
      }
    }

    case DEAL_CARD_TO_DEALER_SUCCESS: {
      if(state.winner) {
        return state
      }
      let turn = state.turn
      let dealerHand = state.dealer.hand.slice().concat(action.cards.cards[0])
      let dealerPoints = dealerHand.reduce(pointsReducer, 0)
      if (dealerPoints > state.player.points && dealerPoints <= 21 ) {
        turn = null
      }
      return {
        ...state,
        dealer: {
          hand: dealerHand,
          points: dealerPoints
        },
        turn
      }
    }

    case FLIP_CARD: {
      let {index} = action
      let cards = state.player.hand.slice()
      cards[index].faceDown = false
      return {
        ...state,
        player: {
          hand: cards,
          points: cards.reduce(pointsReducer, 0)
        }
      }
    }

    case SCORE: {
      let winner = null

      if (state.player.points > 21) {
        winner = DEALER
      }

      if (state.dealer.points > 21) {
        winner = PLAYER
      }

      if (!winner && !state.turn) {
        winner = state.dealer.points > state.player.points ? DEALER : PLAYER
      }

      if (winner) {
        return {
          ...state,
          winner: winner,
          turn: null,
          gameOver: true
        }
      } else {
        return state
      }

    }

    default:
      return state
  }
}