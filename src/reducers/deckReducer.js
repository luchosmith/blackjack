import initialState from '../initialState'
import { DECK_OF_CARDS_LOADING,
         FETCH_CARDS_SUCCESS,
         FETCH_CARDS_ERROR, 
         DEAL_CARDS_SUCCESS,
         DEAL_CARD_TO_DEALER_SUCCESS,
         DEAL_CARD_TO_PLAYER_SUCCESS} from '../actionTypes'

export default function deckReducer(state = initialState.deck, action) {

  switch(action.type) {
    case DECK_OF_CARDS_LOADING: {
      return {...state, loading: true}
    }
    case FETCH_CARDS_SUCCESS: {
      return {...state, ...action.deck, loading: false}
    }
    case FETCH_CARDS_ERROR: {
      return {...state, error: action.error, loading: false}
    }
    case DEAL_CARDS_SUCCESS: {
      return {...state, remaining: action.cards.remaining}
    }
    case DEAL_CARD_TO_DEALER_SUCCESS: {
      return {...state, remaining: action.cards.remaining}
    }
    case DEAL_CARD_TO_PLAYER_SUCCESS: {
      return {...state, remaining: action.cards.cards.remaining}
    }
    default:
      return state
  }
}