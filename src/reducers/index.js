import { combineReducers } from 'redux'
import deckReducer from './deckReducer'
import gameReducer from './gameReducer'

// import dealerReducer from './dealerReducer'
// import playerReducer from './playerReducer'

export default combineReducers({
    deck: deckReducer,
    game: gameReducer
})