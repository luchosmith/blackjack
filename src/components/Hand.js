import React from 'react'
import Card from './Card'
import { PLAYER } from '../actionTypes'

const Hand = ({cards, player, onFlipCard}) => {
  const offset = 10
  const degrees = Math.floor(cards.length/2) * offset
  const rotation = 'rotate(-' + degrees + 'deg)'
  const handStyle = {
    transform: rotation
  }
  const flipCard = player === PLAYER ? onFlipCard : ()=>{}
  return(
    <div className="hand" style={handStyle}>
      {cards.map((card,i) => {
        return (
          <Card key={card.code}
                card={card}
                index={i}
                total={cards.length}
                onFlipCard={flipCard} />
        )
      })}
    </div>)
}

export default Hand
