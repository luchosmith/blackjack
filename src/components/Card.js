import React from 'react'

const renderValue = (value) => {
  let displayValue = value
  if ( ! Number.isInteger(Number.parseInt(value))) {
    displayValue = value.substring(0,1)
  }
  return(<div className="value">{displayValue}</div>)
}

const renderSuit = (suit) => {
  return(<div className={`suit ${suit.toLowerCase()}`}></div>)
}

const renderRotation = (index, total) => {
  const degrees = 15
  const leftOffset = 25
  const topOffset = 5

  let totalDegrees = `rotate(${(index * degrees)}deg)`

  let totalLeftOffset = `${(index * leftOffset)}px`

  let totalTopOffset = `${(index * topOffset)}px`
  let result = {
    transform: totalDegrees,
    left: totalLeftOffset,
    top: totalTopOffset
  }
  return result
}

const renderFaceDown = (card) => {
  if(card.faceDown) {
    return(<div className="card-back"></div>)
  }
  return null
}

const Card = ({card, index, total, onFlipCard}) => {
  return(
    <div className={`card ${card.suit.toLowerCase()}`}
         style={renderRotation(index, total)}
         onClick={()=> onFlipCard(index)}>
      {renderFaceDown(card)}
      <div className="face top-left">
        {renderValue(card.value)} 
        {renderSuit(card.suit)}
      </div>
      <div className="face bottom-right">
        {renderValue(card.value)} 
        {renderSuit(card.suit)}
      </div>
    </div>
  )
}

export default Card