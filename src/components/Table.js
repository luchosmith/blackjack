import React from 'react'
import PropTypes from 'prop-types';
import { DEALER, PLAYER } from '../actionTypes'
import Hand from './Hand'

class Table extends React.Component {

  constructor(props) {
    super(props)
  }

  componentWillReceiveProps(newProps) {
    if(!newProps.winner) {
      if ( (newProps.player.points !== this.props.player.points) ||
           (newProps.dealer.points !== this.props.dealer.points) ) {
        this.props.onPointChange()
      }
      if (newProps.turn === DEALER) {
        this.props.onDealerTurn(this.props.deckId)
      }
    }
  }

  renderLoading() {
    return(<div className="banner">Shuffling...</div> )
  }

  renderWinner() {
    if (this.props.winner === PLAYER) {
      return(<div className="banner">You win!</div> )
    }
    return(<div className="banner">{this.props.winner} wins!</div> )
  }

  renderWelcome() {
    if (!this.props.deckId && !this.props.loading) {
      return(<div className="banner">Blackjack!</div>)
    }
    return null
  }

  renderStartButton() {
    if (!this.props.deckId) {
      return(
        <button className="start" onClick={this.props.onStartButtonClick}>
          START!
        </button>)
    }
    return null
  }

  renderDealButton() {
    if( (this.props.player.hand.length === 0 || this.props.winner)
         && this.props.deckId !== null) {
      return(
        <button
          className="deal"
          onClick={() => this.props.onDealButtonClick(this.props.deckId)}>
          deal cards
        </button>)
    }
    return null
  }

  renderHitButton() {
    if(this.props.turn === PLAYER) {
      return(<button
        className="hit"
        onClick={() => this.props.onHitButtonClick(this.props.deckId)}>
        hit me
      </button>)
    }
    return null
  }

  renderStandButton() {
    if(this.props.turn === PLAYER) {
      return(<button
        className="stand"
        onClick={() => this.props.onStandButtonClick()}
        disabled={this.props.turn !== PLAYER}>
        stand
      </button>)
    }
    return null
  }

  renderPoints(points) {
    let className = points > 21 ? 'over' : ''
    if (points) {
      return(<strong className={className}>{points}</strong>)
    }
    return null
  }

  render() {

    const {
      loading,
      deckId,
      dealer,
      player,
      winner} = this.props

    return (
      <div className="table-top">
        { loading && this.renderLoading() }
        { winner && this.renderWinner() }
        { this.renderWelcome()}
        <div className={`hand-wrapper dealer ${winner === DEALER ? 'win':''}`}>
          <div className="points">
            {this.renderPoints(dealer.points)}
          </div>
          <Hand cards={dealer.hand} player={DEALER} onFlipCard={this.props.onFlipCard} />
        </div>
        <div className={`hand-wrapper player ${winner === PLAYER ? 'win':''}`}>
          <div className="points">
            {this.renderPoints(player.points)}
          </div>
          <Hand cards={player.hand} player={PLAYER} onFlipCard={this.props.onFlipCard} />
        </div>
        <div className="button-container">
          <div className="buttons">
            {this.renderStartButton()}
            {this.renderDealButton()}
            {this.renderHitButton()}
            {this.renderStandButton()}
          </div>
        </div>
      </div>)
  }
}

Table.propTypes = {
  deckId: PropTypes.string,
  dealer: PropTypes.object.isRequired,  
  player: PropTypes.object.isRequired,
  winner: PropTypes.string,
  onStartButtonClick: PropTypes.func.isRequired,
  onDealButtonClick: PropTypes.func.isRequired,
  onPointChange: PropTypes.func.isRequired,
  onDealerTurn: PropTypes.func.isRequired,
  onHitButtonClick: PropTypes.func.isRequired,
  onStandButtonClick: PropTypes.func.isRequired,
  onFlipCard: PropTypes.func.isRequired
}

export default Table