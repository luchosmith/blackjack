/**
 * get a new deck from the api
 * response: {
     "success": true,
     "deck_id": "3p40paa87x90",
     "shuffled": true,
     "remaining": 52
   }
 */
export function shuffle() {
  // hard coded to 1 deck for blackjack (typically it is 6)
  const url = 'https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1'
  return fetch(url)
  .then(response => {
    return response.json()
  })
  .catch(error => {
    console.log(error)
    return {success: false, error}
  })
}
/**
 * 
 * draw n number of cards from the deck
 * response: {
    "success": true,
    "cards": [
        {
            "image": "https://deckofcardsapi.com/static/img/KH.png",
            "value": "KING",
            "suit": "HEARTS",
            "code": "KH"
        },
        {
            "image": "https://deckofcardsapi.com/static/img/8C.png",
            "value": "8",
            "suit": "CLUBS",
            "code": "8C"
        }
    ],
    "deck_id":"3p40paa87x90",
    "remaining": 50
}
 * 
 */
export function draw({deckId, numCards = 1}) {
  const url = `https://deckofcardsapi.com/api/deck/${deckId}/draw/?count=${numCards}`
  return fetch(url)
  .then(response => {
    return response.json()
  })
  .catch(error => {
    console.log(error)
    return {success: false, error}
  })
}