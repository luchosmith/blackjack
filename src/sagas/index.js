import { all, call } from 'redux-saga/effects'
import deckSaga from './deckSaga'

export default function* rootSaga() {
  yield all([
    call(deckSaga)
  ])
}