import { fetchCards } from './deckSaga'
import { DECK_OF_CARDS_LOADING, FETCH_CARDS_SUCCESS, FETCH_CARDS_ERROR } from '../actionTypes/'

describe('deckSaga', () => {

	test('should return expected values for happy path', ()=> {
		let iterator = fetchCards()
		let value = iterator.next().value
		expect(value.PUT).toBeDefined()
		expect(value.PUT.action.type).toEqual(DECK_OF_CARDS_LOADING)
		value = iterator.next().value
		expect(value.CALL).toBeDefined()
		value = iterator.next({success:true,deck_id:1234}).value
		expect(value.CALL).toBeDefined()
		value = iterator.next().value
		expect(value.PUT).toBeDefined()
		expect(value.PUT.action.deck.deck_id).toEqual(1234)
	})
	test('should return expected values for error case', ()=> {
		let value, iterator = fetchCards()
		iterator.next()
		iterator.next()
		value = iterator.next({error:true}).value
		expect(value.PUT).toBeDefined()
		expect(value.PUT.action.type).toEqual(FETCH_CARDS_ERROR)
	})
})