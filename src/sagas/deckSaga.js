import { put, call, all, select, take, takeEvery } from 'redux-saga/effects'
import { delay } from 'redux-saga'
import { getTurn } from '../selectors'
import { DEALER,
         PLAYER,
         FETCH_DECK_OF_CARDS,
         DECK_OF_CARDS_LOADING,
         FETCH_CARDS_SUCCESS,
         FETCH_CARDS_ERROR,
         DEAL_CARDS, 
         DEAL_CARDS_SUCCESS,
         DEAL_CARDS_ERROR,
         DEAL_CARD,
         DEAL_CARD_LOADING,
         DEAL_CARD_TO_PLAYER_SUCCESS,
         DEAL_CARD_TO_DEALER_SUCCESS} from '../actionTypes/'

import { shuffle, draw } from '../api'
 
export function* fetchCards() {
  yield put({ type: DECK_OF_CARDS_LOADING })
  const deck = yield call(shuffle)
  if (deck && deck.success) {
    yield call(delay, 500)
    yield put({ type: FETCH_CARDS_SUCCESS, deck })
  } else {
    yield put({ type: FETCH_CARDS_ERROR, error: 'Error loading cards' })
  }
}

function* dealCards({deckId}) {
  const cards = yield call(draw, {deckId, numCards:4})
  if (cards && cards.success) {
    yield put({ type: DEAL_CARDS_SUCCESS, cards })
  } else {
    yield put({ type: DEAL_CARDS_ERROR, error: 'Error dealing cards' })
  }
}

function* dealCard({deckId}) {
  yield put({ type: DEAL_CARD_LOADING })
  const turn = yield select(getTurn)
  const cards = yield call(draw, {deckId})
  if (cards && cards.success) {
    if (turn === PLAYER) {
      yield put({ type: DEAL_CARD_TO_PLAYER_SUCCESS, cards })
    } else {
      yield call(delay, 500)
      yield put({ type: DEAL_CARD_TO_DEALER_SUCCESS, cards })
    }
  } else {
    yield put({ type: DEAL_CARDS_ERROR, error: 'Error dealing cards' })
  }
}

export function* dealCardSaga() {
  yield takeEvery(DEAL_CARD, dealCard)  
}

export function* dealCardsSaga() {
  yield takeEvery(DEAL_CARDS, dealCards)
}

export function* fetchCardsSaga() {
  yield takeEvery(FETCH_DECK_OF_CARDS, fetchCards)
}

export default function* deckSaga() {
  yield all([
    call(fetchCardsSaga),
    call(dealCardsSaga),
    call(dealCardSaga)
  ])
}