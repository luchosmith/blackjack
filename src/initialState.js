
export default {
  game: {
    loading: false,
    gameOver: true,
    turn: null,
    winner: null,
    dealer: {
      hand: [],
      points: 0
    },
    player: {
      hand: [],
      points: 0
    }
  },
  deck: {
    loading: false,
    deck_id: null,
    shuffled: false,
    remaining: 0,
    error: ''
  }
}
