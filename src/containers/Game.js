import { connect } from 'react-redux'
import Table from '../components/Table'
import { startGame,
         dealCards,
         dealCard,
         playerStand,
         flipCard,
         scoreGame } from '../actions'

const mapStateToProps = (state, ownProps) => {
  return {
    loading: state.game.loading,
    deckId: state.deck.deck_id,
    dealer: state.game.dealer,
    player: state.game.player,
    turn: state.game.turn,
    gameOver: state.game.gameOver,
    winner: state.game.winner
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onStartButtonClick: () => {
      dispatch(startGame())
    },
    onDealButtonClick: (deckId) => {
      dispatch(dealCards(deckId))
    },
    onHitButtonClick: (deckId) => {
      dispatch(dealCard(deckId))
    },
    onStandButtonClick: () => {
      dispatch(playerStand())
    },
    onPointChange: () => {
      dispatch(scoreGame())
    },
    onDealerTurn: (deckId) => {
      dispatch(dealCard(deckId))
    },
    onFlipCard: (index) => {
      dispatch(flipCard(index))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Table)
