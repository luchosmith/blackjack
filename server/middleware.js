const express       = require('express')
const config        = require('../config/config')
const path          = require('path')
const session       = require('express-session')


module.exports = (app) => {

    app.use(session({
        secret: 'keyboard cat',
        cookie: {},
        resave: true,
        saveUninitialized: true
    }))

    app.use(express.static(path.join(__dirname, '/../../public')))
    app.use(express.static(path.join(__dirname, '/../../node_modules')))
}


