const config = {
  dev: 'development',
  test: 'test',
  prod: 'production',
  port: process.env.PORT || 3000,
}

process.env.NODE_ENV = process.env.NODE_ENV || config.dev
config.env = process.env.NODE_ENV

let envConfig

try {
  envConfig = require('./' + config.env)
  envConfig = envConfig || {}
} catch(e) {
  console.log(e)
  envConfig = {}
}

module.exports = Object.assign(config, envConfig)
